// Soal no 1

// buatlah variabel seperti di bawah ini

// var nilai;

// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

// Jawaban soal no 1

var nilai;
nilai = 82;
if (nilai >= 85) {
    console.log('A');
} else if (nilai >= 75 && nilai < 85) {
    console.log('B');
} else if (nilai >= 65 && nilai < 75) {
    console.log('C');
} else if (nilai >= 55 && nilai < 65) {
    console.log('D');
} else if (nilai < 55) {
    console.log('E');
};

// Soal no 2

// buatlah variabel seperti di bawah ini

// var tanggal = 22;
// var bulan = 7;
// var tahun = 2020;

// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

// Jawaban soal no 02

var tanggal = 19;
var bulan = 08;
var tahun = 2003;

switch (bulan) {
    case 05:
        console.log('19 Mei 2003');
        break;
    case 06:
        console.log('19 Juni 2003');
        break;
    case 07:
        console.log('19 Juli 2003');
        break;
    case 08:
        console.log('19 Agustus 2003');
        break;
};

// Soal no 3

// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

// Output untuk n=3 :

// #
// ##
// ###

// Output untuk n=7 :

// #
// ##
// ###
// ####
// #####
// ######
// #######

// Jawaban soal no 3

// n = 3
var output = '';
for (var n = 1; n <= 3; n++) {
    for (var j = 1; j <= n; j++) {
        output += '#';
    }
    console.log(output);
    output = '';
}

// n = 7
for (var n = 1; n <= 7; n++) {
    for (var j = 1; j <= n; j++) {
        output += '#';
    }
    console.log(output);
    output = '';
}

// soal 4

// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
// contoh :

// Output untuk m = 3

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===

// Output untuk m = 5

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript

// Output untuk m = 7

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love programming

// Output untuk m = 10

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===
// 4 - I love programming
// 5 - I love Javascript
// 6 - I love VueJS
// ======
// 7 - I love programming
// 8 - I love Javascript
// 9 - I love VueJS
// =========
// 10 - I love programming

// Jawaban soal no 4
